// package id.ac.ui.cs.advprog.tutorial4.exercise1;
//
// import static org.junit.Assert.assertEquals;
//
// import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.*;
// import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.*;
// import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.*;
// import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.*;
// import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.*;
// import org.junit.*;
//
// public class FactoryTest {
//     private Cheese cheese, mozzaC, parmeC, reggiC, tripleC;
//     private Clams fresh, frozen, timmy;
//     private Dough jumbo, thick, thin;
//     private Sauce mari, plain, plum;
//     private Veggies blackO, cactus, eggplant, garlic, mush, onion, rafflA, redPepP, popeye;
//
//     @Before
//     public void setUp() {
//         mozzaC = new MozzarellaCheese();
//         parmeC = new ParmesanCheese();
//         reggiC = new ReggianoCheese();
//         tripleC = new TripleCheese();
//         fresh = new FreshClams();
//         frozen = new FrozenClams();
//         timmy = new TimmyTheClam();
//     }
//
//     @Test
//     public void testCheeseToStringOutput() {
//         assertEquals(mozzaC.toString(), "Shredded Mozzarella");
//         assertEquals(parmeC.toString(), "Shredded Parmesan");
//         assertEquals(reggiC.toString(), "Reggiano Cheese");
//         assertEquals(tripleC.toString(), "Triple Cheese");
//     }
//
//     public void testClamToStringOutput() {
//         assertEquals(fresh.toString(), "Fresh Clams from Long Island Sound");
//         assertEquals(frozen.toString(), "Frozen Clams from Chesapeake Bay");
//         assertEquals(timmy.toString(), "It's me Timmy the Clam!");
//     }
//
//     public void testDoughToStringOutput() {
//         assertEquals(jumbo.toString(), "The Super Jumbo Mambo Jambo Dough!");
//         assertEquals(thick.toString(), "ThickCrust style extra thick crust dough");
//         assertEquals(thin.toString(), "Thin Crust Dough");
//     }
//
//     public void testVeggiesToStringOutput() {
//         assertEquals(blackO.toString(), "Black Olives");
//         assertEquals(cactus.toString(), "Cactus");
//         assertEquals(eggplant.toString(), "Eggplant");
//         assertEquals(garlic.toString(), "Garlic");
//         assertEquals(mush.toString(), "Mushrooms");
//         assertEquals(onion.toString(), "Onion");
//         assertEquals(rafflA.toString(), "Rafflesia Arnoldi");
//         assertEquals(redPepP.toString(), "Red Pepper");
//         assertEquals(popeye.toString(), "Spinach");
//     }
//
//     public void testSauceToStringOutput() {
//         assertEquals(mari.toString(), "Marinara Sauce");
//         assertEquals(plain.toString(), "Plain Sauce");
//         assertEquals(plum.toString(), "Tomato sauce with plum tomatoes");
//     }
//
// }
