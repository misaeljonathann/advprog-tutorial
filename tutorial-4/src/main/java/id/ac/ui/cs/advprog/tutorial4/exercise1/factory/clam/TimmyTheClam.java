package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class TimmyTheClam implements Clams {

    public String toString() {
        return "It's me Timmy the Clam!";
    }
}
